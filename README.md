# Windows Sandbox Scripts

A collection of scripts/config files for Windows Sandbox, to run on and in the Sandbox.

## Activating the Windows Sandbox Feature

To run these scripts, you'll need to activate Windows Sandbox on your machine. [This article](https://techcommunity.microsoft.com/t5/windows-kernel-internals/windows-sandbox/ba-p/301849) has installation instructions and some basic info on the topic.

Apart from that, you may want to read and understand more about [Windows Sandbox configuration files](https://docs.microsoft.com/en-us/windows/security/threat-protection/windows-sandbox/windows-sandbox-configure-using-wsb-file).

## PDFSandbox

I can't stand installing garbage PDF readers on my main host. They are full of vulnerabilities and install crapware at any opportunity! This config file will open Windows Sandbox and download and install Adobe Reader automatically.

You can simply double click on the config file (.wsb) to run the sandbox!